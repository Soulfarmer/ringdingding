package softwalrusunited.pt.ringdingding;

import softwalrusunited.pt.ringdingding.util.SystemUiHider;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Application;
import android.app.Dialog;
import android.content.DialogInterface;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.InputType;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 *
 * @see SystemUiHider
 */
public class RingDingDing extends Activity {

    /***
     * Sr. Guilherme..
     * layouts est�o em: src/main/res/layout
     * o ficheiro audio em: src/main/res/raw/<play>.mp3
     *
     * strings para tudo o que for apresentado no ecr�
     * aconcelho a colocares em: src/main/res/values/strings.xml
     * (o bot�o chama-se dummy_button, e um id/referencia,
     * se alterares tem cuidado em ver qual � o impacto)
     *
     * ###
     * no layout, � direita consegues ver as properties dos
     * objectos presentes nos layouts, brinca com isso se quiseres mudar cores,
     * strings e formas. Confesso que n�o estou muito dentro disso, mas alguma
     * coisa liga-me ou manda msg, como preferires.
     */

    //todos estes attr j� existiam, n�o mexi
    /**
     * Whether or not the system UI should be auto-hidden after
     * {@link #AUTO_HIDE_DELAY_MILLIS} milliseconds.
     */
    private static final boolean AUTO_HIDE = true;

    /**
     * If {@link #AUTO_HIDE} is set, the number of milliseconds to wait after
     * user interaction before hiding the system UI.
     */
    private static final int AUTO_HIDE_DELAY_MILLIS = 3000;

    /**
     * If set, will toggle the system UI visibility upon interaction. Otherwise,
     * will show the system UI visibility upon interaction.
     */
    private static final boolean TOGGLE_ON_CLICK = true;

    /**
     * The flags to pass to {@link SystemUiHider#getInstance}.
     */
    private static final int HIDER_FLAGS = SystemUiHider.FLAG_HIDE_NAVIGATION;

    /**
     * The instance of the {@link SystemUiHider} for this activity.
     */
    private SystemUiHider mSystemUiHider; //j� existia, n�o mexi
    private String m_Text = ""; // texto para a password
    private MediaPlayer mp = null; //class que gere o ficheiro de audio

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_ring_ding_ding);

        final View controlsView = findViewById(R.id.fullscreen_content_controls);
        final View contentView = findViewById(R.id.fullscreen_content);

        // Set up an instance of SystemUiHider to control the system UI for
        // this activity.
        mSystemUiHider = SystemUiHider.getInstance(this, contentView, HIDER_FLAGS);
        mSystemUiHider.setup();
        mSystemUiHider
                .setOnVisibilityChangeListener(new SystemUiHider.OnVisibilityChangeListener() {
                    // Cached values.
                    int mControlsHeight;
                    int mShortAnimTime;

                    @Override
                    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
                    public void onVisibilityChange(boolean visible) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
                            // If the ViewPropertyAnimator API is available
                            // (Honeycomb MR2 and later), use it to animate the
                            // in-layout UI controls at the bottom of the
                            // screen.
                            if (mControlsHeight == 0) {
                                mControlsHeight = controlsView.getHeight();
                            }
                            if (mShortAnimTime == 0) {
                                mShortAnimTime = getResources().getInteger(
                                        android.R.integer.config_shortAnimTime);
                            }
                            controlsView.animate()
                                    .translationY(visible ? 0 : mControlsHeight)
                                    .setDuration(mShortAnimTime);
                        } else {
                            // If the ViewPropertyAnimator APIs aren't
                            // available, simply show or hide the in-layout UI
                            // controls.
                            controlsView.setVisibility(visible ? View.VISIBLE : View.GONE);
                        }

                        if (visible && AUTO_HIDE) {
                            // Schedule a hide().
                            delayedHide(AUTO_HIDE_DELAY_MILLIS);
                        }
                    }
                });

        //j� existia, n�o mexi
        // Set up the user interaction to manually show or hide the system UI.
        contentView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (TOGGLE_ON_CLICK) {
                    mSystemUiHider.toggle();
                } else {
                    mSystemUiHider.show();
                }
            }
        });

        // Upon interacting with UI controls, delay any scheduled hide()
        // operations to prevent the jarring behavior of controls going away
        // while interacting with the UI.
        //cria-se o bot�o...
        Button btn = ((Button)findViewById(R.id.dummy_button));
        //este metodo(evento) j� existia, n�o mexi...
        btn.setOnTouchListener(mDelayHideTouchListener);
        //adiciona o evento de click...
        btn.setOnClickListener(new View.OnClickListener() {
            // ...e chama um metodo para gerir o click
            // (podia ser feito ali dentro, mas o acesso a outros elementos da class(da Activity) n�o
            // podiam ser chamados explicitamente....mariqueices).
            @Override
            public void onClick(View v) {
                playMe();
            }
        });
    }

    //estes comportamentos s�o do proprio android, existem uns quantos -
    // - procura por Android aplication lifecycle para mais info.
    //j� existia, n�o mexi
    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        // Trigger the initial hide() shortly after the activity has been
        // created, to briefly hint to the user that UI controls
        // are available.
        delayedHide(100);
    }

    //Este metodo gere o que acontece quando clicas no bot�o
    private void playMe(){
        if (mp == null) {
            mp = MediaPlayer.create(getApplicationContext(), R.raw.play);
            mp.setLooping(false);
        }
        if(!mp.isPlaying()){
            mp.start();
        }else{
            mp.stop();
        }
    }

    /**
     * Touch listener to use for in-layout UI controls to delay hiding the
     * system UI. This is to prevent the jarring behavior of controls going away
     * while interacting with activity UI.
     */
    View.OnTouchListener mDelayHideTouchListener = new View.OnTouchListener() {
        // Este metodo � um comportamento do tipo de Aplica��o que criamos.
        // N�o sei detalhes, mas n mexi!
        @Override
        public boolean onTouch(View view, MotionEvent motionEvent) {

            if (AUTO_HIDE) {
                delayedHide(AUTO_HIDE_DELAY_MILLIS);
            }
            return false;
        }
    };

    Handler mHideHandler = new Handler();
    Runnable mHideRunnable = new Runnable() {
        @Override
        public void run() {
            mSystemUiHider.hide();
        }
    };

    /**
     * Schedules a call to hide() in [delay] milliseconds, canceling any
     * previously scheduled calls.
     */
    private void delayedHide(int delayMillis) {
        mHideHandler.removeCallbacks(mHideRunnable);
        mHideHandler.postDelayed(mHideRunnable, delayMillis);
    }


    //Este envento � despetado quando se clica num bot�o
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        //Se for um dos KeyCodes abaixo(Home que n funciona ou Back)...
        if (keyCode == KeyEvent.KEYCODE_HOME || keyCode == KeyEvent.KEYCODE_BACK) {
            //..Cria-se um Alerta (Dialog) ....
            AlertDialog.Builder builder = new AlertDialog.Builder(this);

            //..com este titulo...
            builder.setTitle("Password");

            // ... com um Input field...
            final EditText input = new EditText(this);
            // (Propriedades do input)Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
            input.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
            builder.setView(input);

            //... com um bot�o de OK...
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                //...quando clicas no bot�o OK...
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    //...obvious behaviour xD
                    m_Text = input.getText().toString();
                    if (ValidatePassword(m_Text) == 0) {
                        finish();
                        System.exit(0);
                    }
                }
            });
            //...e um bot�o de cancelar...
            builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });

            //...mostra o dialog/alerta
            builder.show();

            return true;
        }
        return false;
    }

    //...confirma se a password e "admin"
    private int ValidatePassword(String pw) {
        String password = "admin";
        return pw.compareTo(password);
    }
}

